# params
fileName_to_delete = "exercises.jl"
needle_to_find = "chapter"

# Get a list of all items (files and directories) in the parent directory
items = readdir(pwd(), join = true)

# get index of elements within items that contain "..."
chapter_mask = occursin.(needle_to_find, lowercase.(items))

for path in items[chapter_mask]
    # Check if the item is a directory
    if isdir(path)
        # Check if 'exercises.jl' exists in the child directory
        exercise_file = joinpath(path, fileName_to_delete)
        if isfile(exercise_file)
            # Delete 'exercises.jl' if it exists
            println("Deleting $exercise_file")
            rm(exercise_file)
        else
            println("$exercise_file does not exist")
        end
    end
end