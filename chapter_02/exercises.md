# Conceptual Exercises

## 1

a) although p is small, I would expect a flexible method to perform better than an inflexible method because $n$ is large
b) inflexible because there is less of a chance to overfit; a flexible method is more likely to overfit given the relatively small size of n
c) flexible because these methods can better capture the nature of the relationship between **Y** and **X**
d) assuming the irreducible error **is known** it acts as a sort of baseline for how well any procedure will perform. If this value is high, then both methods should perform poorly. However, given that flexible methods *usually* have more accurate predictions, I'd would expect a flexible method to have a lower MSE than a inflexible method.

## 2
a) 

    - type = regression
    - n = 500
    - p = 3
    - Y = ceo salary
b) 

    - type = classification
    - n = 20
    - p = 13
    - Y = success or failure
c)

    - type = regression 
    - n = 52 (weeks per year)
    - p = 3
    - Y = % change in the USD/Euro

## 4
a) 

    1. an experiment to test if a drug successfully treats some disease.
        - Y = has, does not have disease
        - X = various measurements of the observations
        - goal = inference
    2. predict whether a sales opportunity has the characteristics of a winning opportunity
        - Y = stage {won, not won}
        - X = characteristics of the opportunity, such as age, expected close date, size of the opp, etc.
        - goal = prediction
    3. type of beer
        - Y = the type of the beer {stout, lager, etc.}
        - X = characteristics of the beers, such as ABV, color, etc.
        - goal = prediction
b) 

    1. time series forecasting
        - Y = monthly sales
        - X = previous month sales, economic variables, date and time characteristics (there's a package that auto-computes 21 such features), and possibly mixed frequency variables
        - goal = prediction
    2. pay equity by gender
        - Y = salary
        - X = gender, department, job level, etc.
        - goal = inference
    3. what factors drive bookings by analyst
        - Y = daily bookings
        - X = characteristics of the analyst, date variable such as month, etc.
        - inference
c) 

    1. customer segmentation
    2. ...
    3. ...

## 5

In general, flexible methods produce more accurate predictions [when appropriately tuned]. This, is their main advantage. A primary disadvantage is the loss of interpretability (i.e., black box).

A flexible method may be preferred to an inflexible method if the relationships between the supervisor and the predictors are complex and non-linear, if $n$ is *large*, if algorithmic solutions for variable selection are desired (e.g., regularization, pruning, or subset selection).

In general, if the goal of the analysis is inference, that is, the goal is to understand the *true* nature reality (i.e., the relationship between **Y** and **X**) then an inflexible solution is often preferred.

## 6

Leo B. the creator of classification and regression trees penned a seminal paper called something like "the two culture problem." In this paper the author states that there are two cultures, parametric and non-parametric approaches to data analysis.

In classical statistical analysis much effort is given to understanding the nature of the distribution of the response variable. For example, if dealing with count data, perhaps the Possion distribution will appropriately model the data. If the data is highly skewed, perhaps a Gamma distribution would be helpful. What about extreme values, the exponential distribution. These approaches work well if the data is indeed derived from the distribution or if the data closely enough resembles the distribution.

However, often there is no existing distribution that matches the data well enough. In these cases, non-parametric methods such as tree-based algorithms work well. These methods have no preconceptions about the data. They learn from the data. Hence why these methods are often called machine learning methods.

## 7

Had to look these up to refresh my memory:

- the distance between two points on a cartesian plane is $\sqrt{(x_2 + x_1)^2 - (y_2 - y_1)^2}$
- for three coordinates: $\sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2 + (z_2-z_1)^2}$

a)

    1. 3
    2. 2
    3. 3.16
    4. 2.24
    5. 1.41
    6. 1.73

b) green because when K = 1there is only one observation next the point (0,0,0)

c) when K = 3, the nearest observations are red, green, red. The relative frequency of the colors are red = $\frac{2}{3}$ and green = $\frac{1}{3}$. Thus, the prediction when K = 3 is red because red is the most likely class.

d) Large because as K increases the flexibility of the KNN decision boundary increases, thus better matching the nature of the non-linear Bayes decision boundary.